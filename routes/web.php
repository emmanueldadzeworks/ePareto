<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('Dashboard');

//  ROUTE books GROUP
$router->group(['prefix' => 'books'], function () use ($router) {
    $router->get('', 'BooksController@index');
    $router->get('add', 'BooksController@store_view');
    $router->post('add', 'BooksController@store');

    $router->get('generate/report/{report_by}', 'BooksController@report');
});