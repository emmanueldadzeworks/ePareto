@extends('layouts.app')


@section('content')

    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title"> Add Books </h3>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <strong class="text-muted d-block mb-2">Add Book</strong>
        <form  method="post" action="">
        {{csrf_field()}}
        <div class="form-group">
            <div class="input-group mb-3">
                <input name="title" placeholder="title" aria-label="title"  type="text" class="form-control">
            </div>
            <div class="input-group mb-3">
                <input name="authors" placeholder="authors" aria-label="authors"  type="text" class="form-control">
            </div>
            <div class="input-group mb-3">
                <input name="genres" placeholder="genres" aria-label="genres"  type="text" class="form-control">
            </div>
            <div class="input-group mb-3">
                <input name="year" placeholder="year" aria-label="year"  type="text" class="form-control">
            </div>
            <div class="input-group mb-3">
                <input name="ISBN" placeholder="ISBN" aria-label="ISBN"  type="text" class="form-control">
            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Add</button>
                <button type="reset" class="btn btn-default">Cancel</button>
            </div>

        </div>
        </form>
    </div>

@endsection
