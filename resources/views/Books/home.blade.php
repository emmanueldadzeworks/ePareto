@extends('layouts.app')


@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Books Overview</h3>
        <a href="/books/add"> Create Book</a>
        </div>
    </div>
    <!-- End Small Stats Blocks -->
    <div class="row">

        <!-- Books Stats -->
        <div class="card-body p-0 pb-3 text-center">
            <table class="table mb-0">
                <thead class="bg-light">
                <tr>
                    <th scope="col" class="border-0">#</th>
                    <th scope="col" class="border-0">title</th>
                    <th scope="col" class="border-0">authors</th>
                    <th scope="col" class="border-0">genres</th>
                    <th scope="col" class="border-0">year</th>
                    <th scope="col" class="border-0">ISBN</th>
                </tr>
                </thead>
                <tbody>

                    @foreach($books as $book)
                        <tr>
                            <td>-</td>
                            <td>{{$book->title}}</td>
                            <td>{{$book->authors}}</td>
                            <td>{{$book->genres}}</td>
                            <td>{{$book->year}}</td>
                            <td>{{$book->ISBN}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
