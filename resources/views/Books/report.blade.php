@extends('layouts.app')


@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Books Report By {{$report_by}}</h3>
        </div>
    </div>
    <!-- End Small Stats Blocks -->
    <div class="row">

        <!-- Books Stats -->
        <div class="card-body p-0 pb-3 text-center">
            <table class="table mb-0">
                <thead class="bg-light">
                <tr>
                    <th scope="col" class="border-0"> Name of {{$report_by}}</th>
                    <th scope="col" class="border-0">Number Of Books</th>
                </tr>
                </thead>
                <tbody>

                    @foreach($report as $book)
                        <tr>
                            <td>{{$book->$report_by}}</td>
                            <td>{{$book->Number_of_books}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
