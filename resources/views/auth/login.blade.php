
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title> BOOKSHOP</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />


	<!-- ================== END BASE JS ================== -->
    <style>
    body{
        background-color: #68686afa;
    }
    .login .login-content {
    padding: 30px 40px;
    color: #999;
    width: 450px;
    margin: 0 auto;
}
    .login-v2 , .container {
        margin: auto 0px 0px auto !important;
        background: transparent;
        position: fixed;
        bottom: 20px;
        right: 50px;
        width:400px;
    }
    .background-logo{
        background-color:white;
        width: 500px;
        transform: rotate(-35deg);
        height:250px;
        position: absolute;
        top:-250px;
        left:-50px;
    }
    .logo{
        width: 500px;
        height:70px;
        position: absolute;
        top:20px;
        left:50px;
        /* margin: 0 auto; */
        font-size: 3em; 
        font-weight: bold; 
        margin: 6px 0px 0px 3px
    }
    .bg-image{
        /* width: 45%; */
        height:85%;
        position: absolute;
        bottom: 0%;
        left: 15%;
        transform: scaleX(-1);
    }
    
#email, #password {
  width: 100%;
  background-color: #0c0c0c54;
  color: #fff;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 0px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] ,button {
  width: 35%;
  background-color: transparent;
  color: white;
  padding: 6px 20px;
  border: 2px solid #fff;
  cursor: pointer;
}

input[type=submit]:hover , button:hover {
  background-color: #fff;
  color: #480ed4fa;
}
.btn-link{
    color:#fff;float:right;padding:20px;

}
    </style>
</head>
<body>
<img class="bg-image" src="https://pngimage.net/wp-content/uploads/2018/06/jovenes-estudiantes-png-3.png">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <span style="font-size: 2em; font-weight: bold;color:white;"> {{ __('Login') }} </span>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="{{ __('email address') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-6">
                                <input id="password" type="password" placeholder="{{ __('Password') }}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">

                                
                                @if (Route::has('register'))
                                    <a class="btn-link" href="{{ route('register') }}">Register</a>
                                @endif
                                
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
