<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Books;
use DB; 

class BooksController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $books = Books::all();
        return view('Books/home' , ["books" => $books]);

    }

    public function store_view()
    {
        return view('Books/store');
    }

    public function report($report_by = "authors")
    {
        $results =  Books::select($report_by, DB::raw('COUNT('.$report_by.') AS Number_of_books'))
                    ->groupBy($report_by)
                    ->get();
        $data['report_by'] = $report_by;
        $data['report'] = $results;
        
        // return view('Books/report' , $data);
        return view('Books/report' , ['report_by' => $report_by , 'report' => $results]);
    }
    
    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'authors' => 'required',
            'genres' => 'required',
            'year' => 'required',
            'ISBN' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(array(
                'errors' => $validator->getMessageBag()->toArray(),
            ));
        } else {
            if($Books = Books::create($request->all())){
                return redirect('/books')->withSuccessMessage('Updated succesfully'); 
            }else{
                return  false;
            }
        }

    }

     
}